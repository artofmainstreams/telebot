package telebot;

/**
 * Created by frien on 16.08.2016.
 */
public class Sticker {
    private String name;
    private String id;

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
        id = null;

    }

    public void setId(String id) {
        this.id = id;
    }

    public Sticker(String name) {
        this.name = name;
    }
}

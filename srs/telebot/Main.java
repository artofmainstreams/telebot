package telebot;

import org.telegram.telegrambots.TelegramApiException;
import org.telegram.telegrambots.TelegramBotsApi;

/**
 * Created by User on 16.08.2016.
 */
public class Main {
    public static void main(String[] args) {
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        Telebot telebot = Telebot.getTelebot();
        try {
            telegramBotsApi.registerBot(telebot);
        } catch (TelegramApiException e) {

        }
    }

}

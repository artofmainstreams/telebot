package telebot;

import org.telegram.telegrambots.TelegramApiException;
import org.telegram.telegrambots.api.methods.send.SendAudio;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.methods.send.SendSticker;
import org.telegram.telegrambots.api.objects.Message;

import java.io.File;
import java.util.Iterator;
import java.util.Random;

import static telebot.BotResources.stickers;
import static telebot.RequestHandler.requestHandler;
import static telebot.RequestHandler.requestStickerHandler;

/**
 * Created by User on 18.08.2016.
 */
public class BotBehaviour {
    private static Telebot telebot;
    private static Message message;

    public static void toHandleMessage(Telebot tel, Message msg) {
        telebot = tel;
        message = msg;

        System.out.println("Пришло сообщение от: " + msg.getFrom());

        if (message.getSticker() != null) {
            requestStickerHandler(message.getSticker().getFileId());
        } else if (message.hasText()) {
            requestHandler(message.getText());
        }
    }

    public static void sendMessage(String str) {
        //создаём объект, включающий информацию для ответного сообщения
        SendMessage sendMessageRequest = new SendMessage();
        //who should get from the message the sender that sent it
        sendMessageRequest.setChatId(message.getChatId().toString());
        sendMessageRequest.setText(str);

        try {
            System.out.println("Сообщение: " + sendMessageRequest);
            telebot.sendMessage(sendMessageRequest);
        } catch (TelegramApiException e) {
        }
    }

    public static void sendMessageForChris(String str) {
        //создаём объект, включающий информацию для ответного сообщения
        SendMessage sendMessageRequest = new SendMessage();
        //who should get from the message the sender that sent it
        sendMessageRequest.setChatId(message.getChatId().toString());
        sendMessageRequest.enableMarkdown(true);
        sendMessageRequest.setText(str);
        try {
            System.out.println("Сообщение: " + sendMessageRequest);
            telebot.sendMessage(sendMessageRequest);
        } catch (TelegramApiException e) {
        }
    }

    public static void sendPicture(String path) {
        SendPhoto sendPhotoRequest = new SendPhoto();
        sendPhotoRequest.setChatId(message.getChatId().toString());
        File[] files = new File(path).listFiles();
        int index = new Random().nextInt(files.length);
        sendPhotoRequest.setNewPhoto(files[index]); //
        try {
            System.out.println("отправляю картинку");
            telebot.sendPhoto(sendPhotoRequest);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public static void sendAudio(String path) {
        SendAudio sendAudioRequest = new SendAudio();
        sendAudioRequest.setChatId(message.getChatId().toString());
        File[] files = new File(path).listFiles();
        int index = new Random().nextInt(files.length);
        sendAudioRequest.setNewAudio(files[index]);
        System.out.print("запрос принят: ");
        System.out.println(files[index].getName());
        try {
            telebot.sendAudio(sendAudioRequest);
        } catch (TelegramApiException e) {
            System.out.println("не выполнено");
            //e.printStackTrace();
        }
    }
    public static void setupSticker() {
        System.out.println("Ожидаю стикер");
        sendMessage("Ожидаю стикер");

    }

    public static void sendSticker(Sticker sticker) {
        if (sticker.getId() != null) {
            SendSticker sendMessageRequest = new SendSticker();
            sendMessageRequest.setChatId(message.getChatId().toString());
            sendMessageRequest.setSticker(sticker.getId());
            try {
                telebot.sendSticker(sendMessageRequest);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Не могу отправить стикер");
            sendMessage("Не могу отправить стикер");
        }
    }

    public static void removeSticker(String name) {
        Iterator<Sticker> iter = stickers.iterator();

        while (iter.hasNext()) {
            Sticker sticker = iter.next();
            if (name.equals(sticker.getName())) {
                iter.remove();
                System.out.println("Стикер с именем " + sticker.getName() + " успешно удалён");
                sendMessage("Стикер с именем " + sticker.getName() + " успешно удалён");
            }
        }
    }
}
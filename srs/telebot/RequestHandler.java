package telebot;


import java.util.Random;

import static telebot.BotBehaviour.*;
import static telebot.BotResources.*;

class RequestHandler {
    private static int countNewStickers = 0;
    private static int countRemoveStickers = 0;

    static void requestStickerHandler(String id) {
        if (countNewStickers != 0) {
            stickers.get(stickers.size() - countNewStickers).setId(id);
            countNewStickers--;
        }
    }

    static void requestHandler(String request) {
        request = request.toLowerCase().trim().replaceAll("\\.|\\,|\\?|\\!|\\*|\\&|\\^|\\;|\\:|\\-|\\)|\\=", "");
        String[] splitRequest = request.split(" ", 0);

        boolean isRequestForMe = false;
        boolean isDestroy = false;
        boolean isSticker = false;
        boolean isDontGive = false;
        boolean isRemoveSticker = false;
        for (String requestString : splitRequest) {
            isRequestForMe |= welcomeHandler(requestString);
            isSticker |= stickerHandler(requestString);
            isDontGive |= dontGiveHandler(requestString);
            isRemoveSticker |= removeStickerHandler(requestString);
            isDestroy |= destroyHandler(requestString);
            respondHandler(requestString, morning, answerForMorning);
            respondHandler(requestString, night, answerForNight);
            respondHandler(requestString, thanks, answerForThanks);
            krasavaHandler(requestString);
            respondHandler(requestString, lol, answerForLol);
            respondHandlerForChris(requestString, compliment, answerForCompliment);
        }

        for (String requestString : splitRequest) {
            if (isRequestForMe && !isDontGive) {
                if (isRemoveSticker) {
                    removeNamedSticker(requestString);
                } else if (isSticker) {
                    setupStickerHandler(requestString);
                    sendNamedSticker(requestString);
                    showAllStickersHandler(requestString);
                }
                if (isDestroy) {
                    sendDangerousMessage(requestString, answerForDestroy);
                }
                howAreYouHandler(requestString);
                pictureHandler(requestString);
                screensaverHandler(requestString);
                audioHandler(requestString);
                helpHandler(requestString);
            } else if (isRequestForMe) {
                respondHandler(requestString, dontGive, answerForDontGive);
            }
        }
    }

    private static void sendDangerousMessage(String requestString, String[] answer) {
        if (requestString.equals("@friendboy1")) {
            sendMessage("Не прокатит");
        } else if (requestString.startsWith("@")) {
            int index = new Random().nextInt(answer.length);
            sendMessage(requestString + " " + answer[index]);
        }
    }

    private static boolean destroyHandler(String requestString) {
        for (String compareString : destroy) {
            if (requestString.equals(compareString)) {
                return true;
            }
        }
        return false;
    }

    private static void howAreYouHandler(String requestString) {
        for (String compareString : how) {
            if (requestString.equals(compareString)) {
                sendMessage("Шикос, а у тебя?)");
            }
        }
    }

    private static void helpHandler(String requestString) {
        for (String compareString : help) {
            if (requestString.equals(compareString)) {
                sendMessage("name: telebot\n" +
                        "Что я умею:\n" +
                        "Добавлять стикеры в свой арсенал\n" +
                        "Отправлять стикеры в чат\n" +
                        "Удалять стикеры из своего арсенала\n" +
                        "Отправлять картиночки в чат\n" +
                        "Отправлять скринсейверы в чат\n" +
                        "Отправлять песенки в чат\n" +
                        "create by Андрей Хромов");
            }
        }
    }

    private static void audioHandler(String requestString) {
        for (String compareString : audio) {
            if (requestString.equals(compareString)) {
                sendMessage("Запрос на песню принят, ожидай");
                sendAudio("C:\\Java\\IDEA\\telebot\\resources\\audio");
                System.out.println("включаю песню");
            }
        }
    }

    private static void removeNamedSticker(String requestString) {
        for (Sticker sticker : stickers) {
            if (requestString.equals(sticker.getName())) {
                removeSticker(sticker.getName());
            }
        }
    }

    private static boolean removeStickerHandler(String requestString) {
        if (requestString.equals("удали")) {
            return true;
        }
        return false;
    }

    private static void showAllStickersHandler(String requestString) {
        for (String compareString : showAllStickers) {
            if (requestString.equals(compareString)) {
                String str = "Стикеры:";
                for (Sticker sticker : stickers) {
                    str += "\n» " + sticker.getName();
                }
                sendMessage(str);
            }
        }
    }

    private static boolean dontGiveHandler(String requestString) {
        for (String compareString : dontGive) {
            if (requestString.equals(compareString)) {
                return true;
            }
        }
        return false;
    }

    private static void setupStickerHandler(String requestString) {
        int index = requestString.indexOf('\"');
        if (index != -1 && requestString.charAt(requestString.length() - 1) != requestString.charAt(index)) {
            sendMessage("Не могу обработать имя стикера");
            return;
        }
        if ((requestString.length() - 1) == index) {
            return;
        }
        if (index != -1) {
            String stickerName = requestString.substring(1, requestString.length() - 1);
            boolean isExist = false;
            for (Sticker sticker : stickers) {
                if (sticker.getName().equals(stickerName)) {
                    isExist = true;
                    System.out.println("true");
                }
            }
            if (!isExist) {
                sendMessage("Ожидаю стикер");
                stickers.add(new Sticker(stickerName));
                countNewStickers++;
            }
        }
    }

    private static void sendNamedSticker(String requestString) {
        for (Sticker sticker : stickers) {
            if (requestString.equals(sticker.getName())) {
                sendSticker(sticker);
            }
        }
    }

    private static boolean stickerHandler(String requestString) {
        for (String compareString : sticker) {
            if (requestString.equals(compareString)) {
                return true;
            }
        }
        return false;
    }


    private static void respondHandler(String requestString, String[] ask, String[] answer) {
        for (String compareString : ask) {
            if (requestString.equals(compareString)) {
                int index = new Random().nextInt(answer.length);
                sendMessage(answer[index]);
            }
        }
    }

    private static void respondHandlerForChris(String requestString, String[] ask, String[] answer) {
        for (String compareString : ask) {
            if (requestString.equals(compareString)) {
                int index = new Random().nextInt(answer.length);
                sendMessageForChris(answer[index]);
            }
        }
    }

    private static boolean welcomeHandler(String requestString) {
        for (String compareString : botName) {
            if (requestString.equals(compareString)) {
                return true;
            }
        }
        return false;
    }

    private static void pictureHandler(String requestString) {
        for (String compareString : picture) {
            if (requestString.equals(compareString)) {
                sendPicture("C:\\Java\\IDEA\\telebot\\resources\\picture");
                //sendMessage("Держи: https://yadi.sk/d/1CRa8CObuSBwU");
            }
        }
    }

    private static void screensaverHandler(String requestString) {
        for (String compareString : screensaver) {
            if (requestString.equals(compareString)) {
                sendPicture("E:\\Pictures\\Screensavers");
            }
        }
    }

    private static void krasavaHandler(String requestString) {
        for (String compareString : krasava) {
            if (requestString.equals(compareString)) {
                sendMessage("Красава!");
            }
        }
    }
}

package telebot;

import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import static telebot.BotBehaviour.*;

/**
 * Created by artofmainstreams on 19.08.2016.
 */
public class Telebot extends TelegramLongPollingBot {
    private static Telebot telebot;

    public static Telebot getTelebot() {
        if (telebot == null) {
            telebot = new Telebot();
        }
        return telebot;
    }

    @Override
    public void onUpdateReceived(Update update) {
        if(update.hasMessage()){
            Message message = update.getMessage();
            toHandleMessage(telebot, message);
        }
    }

    @Override
    public String getBotUsername() {
        return BotConfig.BOT_USERNAME;
    }

    @Override
    public String getBotToken() {
        return BotConfig.BOT_TOKEN;
    }
}
